ruby-recaptcha (5.12.3-2) unstable; urgency=medium

  * Reupload to unstable
  * Drop obsolete X{S,B}-Ruby-Versions fields

 -- Pirate Praveen <praveen@debian.org>  Sun, 18 Jun 2023 12:13:36 +0530

ruby-recaptcha (5.12.3-1) experimental; urgency=medium

  [ Utkarsh Gupta ]
  * Team Upload.
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.4.1, no changes needed.
  * Update standards version to 4.5.0, no changes needed.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.
  * Update standards version to 4.6.1, no changes needed.

  [ Vinay Keshava ]
  * update d/watch
  * New upstream version 5.12.3

 -- Vinay Keshava <vinaykeshava@disroot.org>  Wed, 08 Mar 2023 23:44:53 +0530

ruby-recaptcha (4.11.1-2) unstable; urgency=medium

  * Remove unneeded dependency on rails, add ruby-json dependency
  * Bump Standards-Version to 4.3.0 (no changes needed)
  * Update debhelper compatibility level to 11~

 -- Pirate Praveen <praveen@debian.org>  Wed, 06 Mar 2019 17:03:24 +0530

ruby-recaptcha (4.11.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 4.11.1
  * Bump debhelper compatibility level to 11
  * Declare complaince with Debian Policy 4.2.0
  * Update VCS urls to point to salsa
  * Update years of upstream copyright
  * debian/copyright: use secure urls in Format and Source fields
  * Update Debian packaging copyright

 -- Lucas Kanashiro <lucas.kanashiro@collabora.co.uk>  Fri, 24 Aug 2018 15:20:37 -0300

ruby-recaptcha (3.2.0-3) unstable; urgency=medium

  * Make the package officially team maintained (Closes: #843437)
  * Add myself as uploader

 -- Pirate Praveen <praveen@debian.org>  Sun, 19 Feb 2017 13:04:41 +0530

ruby-recaptcha (3.2.0-2) unstable; urgency=medium

  * Team upload
  * Reupload to unstable
  * Check gemspec dependencies during build

 -- Pirate Praveen <praveen@debian.org>  Sat, 09 Jul 2016 18:25:17 +0530

ruby-recaptcha (3.2.0-1) experimental; urgency=medium

  * Team upload
  * New upstream major release
  * Remove patch gemspec_without_git.patch (gem2deb can handle it now)

 -- Pirate Praveen <praveen@debian.org>  Thu, 16 Jun 2016 16:04:41 +0530

ruby-recaptcha (0.4.0-1) unstable; urgency=medium

  * New upstream release:
    - Support for ReCAPTCHA v2.
  * Fix rails dependency to be >> 4.1, due to #754981.
  * debian/patches: Added gemspec_without_git to deal with the infamous `git
    ls-files` usual command, invalid in our chroots, and thus ship a valid
    gemspec (LP: #1400564).
  * debian/watch: Added.
  * debian/control: Bumped to 3.9.6 (no changes).

 -- David Martínez Moreno <ender@debian.org>  Tue, 28 Jul 2015 11:00:49 -0700

ruby-recaptcha (0.3.6-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Replace ruby1.9 dependency with ruby metapackage.

 -- Christian Hofstaedtler <zeha@debian.org>  Wed, 09 Apr 2014 16:00:42 +0200

ruby-recaptcha (0.3.6-2) unstable; urgency=low

  * debian/control: Remove ruby-pkg-tools from Build-Depends (closes:
    #737179).

 -- David Martínez Moreno <ender@debian.org>  Thu, 30 Jan 2014 23:52:57 -0800

ruby-recaptcha (0.3.6-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Bumped Standards-Version (no change).
    - Changed a dependency on ruby1.8 to ruby1.9.
    - Bumped Rails dependency to >> 3.
    - Added Vcs-* fields.

 -- David Martínez Moreno <ender@debian.org>  Wed, 08 Jan 2014 23:24:09 -0800

ruby-recaptcha (0.3.2+git20121017-1) unstable; urgency=low

  * Initial release (closes: #690754).

 -- David Martínez Moreno <ender@debian.org>  Thu, 18 Oct 2012 12:25:36 -0700
